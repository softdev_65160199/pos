/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.service.ProductService;

/**
 *
 * @author sarit
 */
public class TestProductService {
    public static void main(String[] args) {
        ProductService pd = new ProductService();
        for(Product product : pd.getProducts()){
            System.out.println(product );
        }
        
       System.out.println( pd.getById(4));
       Product newpd = new Product("Tea", 30, "Tal", "0123", "H", 1);
       pd.addNew(newpd);
       for(Product product : pd.getProducts()){
            System.out.println(product );
        }
       
       Product uppd1 = pd.getById(4);
       uppd1.setName("Butter Cake");
       pd.update(uppd1);
       
       pd.delete(pd.getById(3));
       for(Product product : pd.getProducts()){
            System.out.println(product );
        }
        
        
    }
}
